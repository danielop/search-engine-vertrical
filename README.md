# Search Engine - Vertrical

This project was created to send my test to Vetrical company

## Client Side Structure

In the client-side directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Server Side Structure

In the server-side directory, you can run:

### `npm start`

Runs the server in the development mode in http://localhost:3001

## Creator

Daniel Lopez [LinkedIn](https://www.linkedin.com/in/danlopezcol/).
