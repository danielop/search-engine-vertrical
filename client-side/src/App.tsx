import {
  BrowserRouter,
  Route,
  Routes
} from "react-router-dom";

import Search from './pages/search'
import Detail from './pages/detail'

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Search />}/>
        <Route path="/detail" element={<Detail />} />
      </Routes>
    </BrowserRouter>
  );
}
