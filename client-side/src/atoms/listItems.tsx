import {atom} from 'jotai';

const listItems:any = atom([]);
const searchText:any = atom('');
const selectedItem:any = atom(null);

const allAtamos = {
  listItems,
  searchText,
  selectedItem
}

export default allAtamos