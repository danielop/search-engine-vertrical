import './styles.css'
import { searchAtom } from '../../atoms';
import { useAtom } from 'jotai'

interface ItemData {
  photo:string,
  title:string,
  shortDescription:string
}

interface Props {
  item:ItemData,
  key:string,
  onClick:any
}

const ItemList = (props:Props) => {
  const item:ItemData = props.item
  const [, setSelectedItem] = useAtom<any,any,any>(searchAtom.selectedItem)

  return <div className='Item_Container'>
    <div className='Item_Photo_Container'>
      <img
        alt='Test item'
        src={item?.photo || 'https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg'}
        className='Item_Photo'
      />
    </div>
    <div className='Item_Row_Description'>
      <p className='Item_Title' onClick={() => {
        setSelectedItem(item)
        props.onClick('/detail')
      }}>
        {item?.title}
      </p>
      <p className='Item_Description'>
        {item?.shortDescription}
      </p>
    </div>
  </div>
}

export default ItemList