import { render, screen } from '@testing-library/react';
import ItemList from './index';

test('renders item component', () => {
  const props:any = {
    item: {title: 'Whola this is my title', photo: 'I dont have photo', description: 'Long description', shortDescription: 'It is short'}
  }
  render(<ItemList {...props} />);
  expect(screen.getByText(/Whola this is my title/i)).toBeInTheDocument();
  expect(screen.getByText(/It is short/i)).toBeInTheDocument();
});
