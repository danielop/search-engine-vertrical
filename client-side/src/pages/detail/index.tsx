import './styles.css'
import { useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { searchAtom } from '../../atoms';
import { useAtom } from 'jotai'

const Detail = () => {
  const navigate = useNavigate();
  const [selectedItem, setSelectedItem] = useAtom<any,any,any>(searchAtom.selectedItem)

  useEffect(() => {
    if (!selectedItem) {
      navigate('/')
    }
    return () => setSelectedItem(null)
    //eslint-disable-next-line
  }, [])

  return <div className='Page_Detail_Container'>
    <button
      className='Back_Button'
      onClick={() => navigate('/')}
    >
      {'< Back'}
    </button>
    <p className='Page_Title'>
      {selectedItem?.title}
    </p>
    <p className='Page_Description'>
      {selectedItem?.description}
    </p>
    <img
      alt='Test item'
      src={selectedItem?.photo}
      className='Page_Photo'
    />
  </div>
}

export default Detail