import './styles.css'
import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import ItemList from '../../components/itemList'
import { searchAtom } from '../../atoms';
import Api from '../../utils/api'
import { useAtom } from 'jotai'

interface Form {
  preventDefault: any,
}

const Search = () => {
  const navigate = useNavigate();
  const [searchText, setSearchText] = useState('')
  const [loading, setLoading] = useState(false)
  const [foundItems, setFoundItems] = useAtom<any,any,any>(searchAtom.listItems)
  const [oldlSeachText, setOldlSeachText] = useAtom<any,any,any>(searchAtom.searchText)

  const onSearch = async (e:Form) => {
    try {
      e.preventDefault()
      setFoundItems([])
      if (!searchText) {
        alert('Please fill input and try again')
      } else {
        setLoading(true)
        // Using timeout to simulate a real request with delay
        const response:any = await new Promise((resolve, reject) => {
          setTimeout(() => {
            Api.post('/admin/postMySearch', {searchText}).then(resolve).catch(reject)
          }, 2000);
        })
        if (response.ok) {
          setFoundItems(response.data)
          setOldlSeachText(searchText)
          if (response.data.length < 1) {
            throw new Error('No results match your search :( Try again')
          }
        } else {
          throw String(response.data)
        }
      }
      setSearchText('')
    } catch (e:any) {
      alert((e?.message || e) || 'Request to search fails')
      setOldlSeachText('')
      setFoundItems([])
    } finally {
      setLoading(false)
    }
  }

  const renderItems = () => {
    return foundItems.map((item:any, index:number) => <ItemList
      key={'item-list-' + index}  
      item={item}
      onClick={() => navigate('/detail')}
    />)
  }

  return <div className='Page_Container'>
    <form onSubmit={onSearch} className='Form_Container'>
      <p className='Form_Container_Title'>
        Welcome to Search Engine - Vertrical
      </p>
      <input
        placeholder="Enter your text to search ..."
        type='text'
        value={searchText}
        onChange={({target}) => setSearchText(target.value)}
        className='Form_Container_Input'
        maxLength={100}
      />
      <input
        type='submit'
        value={loading ? 'Loading ...' : 'Search'}
        className='Form_Container_Submit'
        disabled={loading}
      />
    </form>
    {foundItems.length > 1 ? <>
      <p className='List_Container_Title'>
        Results of your search "{oldlSeachText}"
      </p>
      <div className='List_Container'>
        {renderItems()}
      </div>
    </>  : null}
  </div>
}

export default Search