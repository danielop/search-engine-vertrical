import { create } from 'apisauce'

const api = create({
    baseURL: 'http://localhost:3001',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    timeout: 40000
})

api.axiosInstance.interceptors.request.use(async (config:any) => {
  // Do something before request is sent
  config.headers['x-tenant'] = window.location.hostname;
  config.headers['Authorization'] = `Bearer 1234asdf`;
  
  return config;
}, function (error:any) {
  // Do something with request error
  return Promise.reject(error);
});


api.axiosInstance.interceptors.response.use(undefined, async (error:any) => {
  const errorResponse =  error.response;

  if (errorResponse && errorResponse.status === 401 && !!error.config &&  typeof error.config.canRetry  === 'undefined' ) {
    error.config.headers['Authorization'] = `Bearer 1234asdf`;
    error.config.canRetry = false;
    return api.axiosInstance.request(error.config)
  }
  throw error;
})




export const axios = api.axiosInstance

export default api