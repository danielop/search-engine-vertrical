# Server side

This project was created with NodeJS

## Client Side Structure

In this project you can run:

### `npm start`

Runs the server in the development mode in http://localhost:3001

### `npm build`

Create dist folder with all typscript files converted in javascript files

### `npm serve`

Runs the server in the development mode in http://localhost:3001

## Creator

Daniel Lopez [LinkedIn](https://www.linkedin.com/in/danlopezcol/).
