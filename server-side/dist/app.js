"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require('dotenv').config();
const helmet = require('helmet');
const cors = require('cors');
const app = (0, express_1.default)();
// Import All routes
const adminRouter = require('./modules/admin/router');
const allowlist = [
    'localhost:3000'
];
const corsOptions = function (req, callback) {
    let corsOptions, flag = false;
    allowlist.forEach((item) => {
        const originValue = req.header('Origin');
        if (originValue && originValue.indexOf(item) > -1) {
            flag = true;
        }
    });
    if (flag) {
        corsOptions = { origin: true };
    }
    else {
        corsOptions = { origin: false };
    }
    callback(null, corsOptions);
};
// Initial configuration
app.use(helmet());
app.use(cors(corsOptions));
app.use(express_1.default.urlencoded({ extended: false }));
app.use(express_1.default.json({ type: ['application/json', 'text/plain'] }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
// Return error without route
app.get("/", (req, res) => {
    res.status(200).send('Error: You are lost.');
});
// Config routes
app.use('/admin', adminRouter);
module.exports = app;
