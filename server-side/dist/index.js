"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app = require('./app');
const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log(`Server running in http://localhost:${port}`);
});
