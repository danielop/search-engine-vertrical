"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DummyData = require('../../utils/dummyData');
const postMySearch = (req, res) => {
    try {
        const body = req.body;
        let filteredData = [];
        if (body.searchText) {
            // Start search in each object
            filteredData = DummyData.filter((dataItem) => {
                const titleObject = dataItem.title.toLowerCase().replace(/ /g, '');
                const searchText = body.searchText.toLowerCase().replace(/ /g, '');
                return titleObject.indexOf(searchText) > -1;
            });
        }
        else {
            throw new Error('Search request has no value');
        }
        res.status(200).send(filteredData);
    }
    catch (e) {
        res.status(400).send(e || new Error('Search fails - Fatal error'));
    }
};
module.exports = {
    postMySearch
};
