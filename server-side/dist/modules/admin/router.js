"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const Controller = require('./controller');
const api = (0, express_1.Router)();
const MiddlewareAuth = require('../../middlewareAuth');
api.post('/postMySearch', MiddlewareAuth, Controller.postMySearch);
module.exports = api;
