import express, {Application, Request} from 'express';

require('dotenv').config();

const helmet:any = require('helmet');
const cors:any = require('cors');
const app:Application = express();


// Import All routes
const adminRouter = require('./modules/admin/router');

const allowlist = [
  'localhost:3000'
];
const corsOptions = function (req:Request, callback:any) {
  let corsOptions, flag = false;
  allowlist.forEach((item) => {
    const originValue:string | undefined = req.header('Origin')
    if (originValue && originValue.indexOf(item) > -1) {
      flag = true;
    }
  });
  if (flag) {
    corsOptions = { origin: true }
  } else {
    corsOptions = { origin: false }
  }
  callback(null, corsOptions)
};

// Initial configuration
app.use(helmet());
app.use(cors(corsOptions));
app.use(express.urlencoded({extended: false}));
app.use(express.json({ type: ['application/json', 'text/plain'] }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

// Return error without route
app.get("/", (req, res) => {
  res.status(200).send('Error: You are lost.');
});

// Config routes
app.use('/admin', adminRouter);

module.exports = app;