import { Application } from 'express';

const app:Application = require('./app');
const port:string | number = process.env.PORT || 3001;

app.listen(port, () => {
	console.log(`Server running in http://localhost:${port}`);
});