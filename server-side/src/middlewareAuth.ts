import {Request, Response, NextFunction} from 'express';

const getAuthToken = (req:Request, res:Response, next:NextFunction) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    req.headers.authToken = req.headers.authorization.split(' ')[1];
  } else {
    req.headers.authToken = '';
  }
  next();
};

const checkIfAuthenticated = (req:Request, res:Response, next:NextFunction) => {
 getAuthToken(req, res, async () => {
    try {
      const { headers } = req;
      if (headers.authToken !== '1234asdf'){
        throw new Error('Credentials are invalid')
      }
      return next();
    } catch (e) {
      return res.status(401).send({ error: 'It\'s not authorized' });
    }
  });
};

module.exports = checkIfAuthenticated