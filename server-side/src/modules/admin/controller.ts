import {Request, Response} from 'express';

interface SearchRequest {
  searchText: string
}

const DummyData:object[] = require('../../utils/dummyData');

const postMySearch = (req:Request, res:Response) => {
  try {
    const body:SearchRequest = req.body;
    let filteredData:object[] = []
    if (body.searchText) {
      // Start search in each object
      filteredData = DummyData.filter((dataItem:any) => {
        const titleObject:string = dataItem.title.toLowerCase().replace(/ /g, '')
        const searchText:string = body.searchText.toLowerCase().replace(/ /g, '')
        return titleObject.indexOf(searchText) > -1
      })
    } else {
      throw new Error('Search request has no value')
    }
    res.status(200).send(filteredData);
  } catch (e) {
    res.status(400).send(e || new Error('Search fails - Fatal error'));
  }
}

module.exports = {
  postMySearch
}