import {Router} from 'express';

const Controller = require('./controller');
const api:Router = Router();
const MiddlewareAuth = require('../../middlewareAuth');

api.post('/postMySearch', MiddlewareAuth, Controller.postMySearch);

module.exports = api;